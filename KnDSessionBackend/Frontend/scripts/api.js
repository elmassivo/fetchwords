function createSession(newBoardSeed, revealedCardIds){
    var createParams = {
        boardSeed : newBoardSeed,
        revealedCards : revealedCardIds
    };
    toggleLoadingOverlay();
    $.post('/api/Session/CreateSession', createParams)
    .done(function(response){
        if(response != null && response.success && response.data != null)
        {
            session = response.data.sessionId;
            location.hash = createHashString();
            var shareUrl = document.location.origin + document.location.pathname + "#session=" + session;
            $('#sessionIDText').text(session);
            $('#inviteURL').val(shareUrl);
            $('#inviteURL').text(shareUrl);
            showInviteFriendsScreen();
            updateBoardFromSession(response.data.sessionBoard);
            getSessionUpdateLoop();
        }
        else
        {
            //TODO: throw an error here
            return false;
        }
        toggleLoadingOverlay();
    })
    .fail(function(){
        alert("Could not create session, unable to connect to server.");
        toggleLoadingOverlay();
    });
}

function updateBoardFromSession(sessionBoard){
    if(seed !== sessionBoard.boardSeed){
        seed = sessionBoard.boardSeed;
        $('#seed').val(seed);
        location.hash = createHashString();
        fire();
    }
    $.each(sessionBoard.revealedCards, function(index,item){
        sessionClickTile(''+item);
    });
}

function performMove(sessionId, revealedCardList){
    var performParams = {
        sessionId: sessionId,
        revealedCards: revealedCardList
    };
    $.post('/api/Session/PerformMove', performParams)
    .done(function(response){
        if(response != null && response.success && response.data != null)
        {
            return response.data;
        }
        else
        {
            //TODO: throw an error here
            return false;
        }
    })
    .fail(function(){
        connectionFailedModal();
    });;
}

var sessionRetry = 0;
function getSession(sessionId){
    $.get('/api/Session/GetSession?sessionId='+sessionId)
    .done(function(response){
        if(response != null && response.success && response.data != null)
        {
            updateBoardFromSession(response.data.sessionBoard);
            var shareUrl = document.location.origin + document.location.pathname + "#session=" + session;
            $('#sessionIDText').text(session);
            $('#inviteURL').val(shareUrl);
            $('#inviteURL').text(shareUrl);
        }
        else
        {
            sessionRetry++;
            if(sessionRetry>20) {
                connectionFailedModal()
            }
            return false;
        }
        hideLoadingOverlay();
    })
    .fail(function(){
        sessionRetry++;
        if(sessionRetry > 20) {
            connectionFailedModal();
        }
        hideLoadingOverlay();
    });;
}

function updateSeed(sessionId, newSeed){
    var updateParams = {
        sessionId: sessionId,
        newSeed: seed
    };
    toggleLoadingOverlay();
    $.post('/api/Session/UpdateSeed', updateParams)
    .done(function(response){
        if(response != null && response.success && response.data != null)
        {
            seed = '';
            updateBoardFromSession(response.data.sessionBoard);
            location.hash = createHashString();
        }
        else
        {
            //TODO: throw an error here
            return false;
        }
        toggleLoadingOverlay();
    })
    .fail(function(){
        connectionFailedModal();
    });;

}

function connectionFailedModal(){
    createConfirmDialog(
        "Server connection lost",
        "Attempt to reconnect or switch to manual mode?",
        "Reconnect","Manual Mode",
        function(){
            clearTimeout(getSessionLoop);
            location.reload();
        },
        manualMode());
}