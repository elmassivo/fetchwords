function hideWelcomeModal() {
    $('#navigationMenu').fadeOut(500);
    setTimeout(() => {
        $('.modalOverlay').hide();
        $('#inviteFriendsScreen').siblings(".drawer").show();
        $('#inviteFriendsScreen').hide();
    }, 600);
}

function showWelcomeScreen() {
    $('.modalOverlay').show();
    $('#navigationMenu').fadeIn(500);
    $('.hamburger').removeClass('is-active');
    $('#menu').fadeOut(500);
    $('#seedBox').fadeOut(500);
    menuIsOpen = false;
}

function closeMenu(){
    hideWelcomeModal();
}

function showInviteFriendsScreen() {
    $('#inviteFriendsScreen').siblings(".drawer").fadeOut(200);
    $('#inviteFriendsScreen').fadeIn(200);
}

$('.modalOverlay').click(function(){
    if(menuIsOpen) {
        $('.modalOverlay').hide();
        $('.hamburger').click();
    }
});

$('.hamburger').click(function(){
	if(menuIsOpen) {
		$('.hamburger').removeClass('is-active');
		$('#menu').fadeOut(500);
		$('.modalOverlay').hide();
		menuIsOpen = false;
	}
	else{
        $('#navigationMenu').fadeOut(500);
		$('.hamburger').addClass('is-active');
		$('#menu').fadeIn(500);
		$('.modalOverlay').show();
		menuIsOpen = true;
	}
});

function joinGameSession(){
	session = $('#txtSessionId').val();
    location.hash = createHashString();
	getSessionUpdateLoop();
	closeMenu();
};


function toggleLoadingOverlay(){
    $('.loadingModalOverlay').toggle()
    $('#loadingModal').toggle();
}

function hideLoadingOverlay(){
    if(!confirmModalVisible){
        $('.loadingModalOverlay').hide();
        $('#loadingModal').hide();
    }
}

function showLoadingOverlay(){
    $('.loadingModalOverlay').show();
    $('#loadingModal').show();
}

var confirmModalVisible = false;
function createConfirmDialog(titleText, ContentText, LeftBtnText, RightBtnText, ConfirmCallback, CancelCallback){
    $('.confirmModalTitle').text(titleText);
    $('.confirmModalText').text(ContentText);
    
    if(LeftBtnText != null){
        $('.confirmModalBtn.Left').text(LeftBtnText);
    }
    else {
        $('.confirmModalBtn.Left').hide();
    }
    if(RightBtnText != null) {
        $('.confirmModalBtn.Right').text(RightBtnText);
    }
    else{
        $('.confirmModalBtn.Right').hide();
    }

    confirmModalVisible = true;
    $('.confirmModal').fadeIn(200);
    $('.loadingModalOverlay').fadeIn(200);

    $('.confirmModalBtn.Left').click(function(){
        if(ConfirmCallback) { ConfirmCallback(); }
        $('.confirmModal').fadeOut(200);
        $('.loadingModalOverlay').fadeOut(200);   
        hideMenu();
        confirmModalVisible = false;
    });

    $('.confirmModalBtn.Right').click(function(){
        if(CancelCallback) { CancelCallback(); }
        $('.confirmModal').fadeOut(200);
        $('.loadingModalOverlay').fadeOut(200);
        confirmModalVisible = false;
    });
}

function hideMenu(){
    $('.hamburger').removeClass('is-active');
    $('#menu').fadeOut(500);
    $('.modalOverlay').hide();
    menuIsOpen = false;
}

function toggleSeedBox(){
    $('#seedBox').toggle("fade", 500);
}

function manualMode(){
    toggleSeedBox();
    closeMenu();
    clearTimeout(getSessionLoop);
    location.hash='';
    session = '';
}