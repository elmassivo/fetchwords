﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KodeNamesBackend.Models
{
    public class UpdateSeedParameters
    {
        public string sessionId { get; set; }
        public string newSeed { get; set; }
    }
}