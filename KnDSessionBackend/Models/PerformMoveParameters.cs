﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KodeNamesBackend.Models
{
    public class PerformMoveParameters
    {
        public PerformMoveParameters()
        {
            revealedCards = new List<int>();
        }
        public string sessionId { get; set; }
        public List<int> revealedCards { get; set; }
    }
}