﻿using System;
using System.Collections.Generic;

namespace KodeNamesBackend.Models
{
    public class GameSession
    {
        public GameSession()
        {
            sessionBoard = new GameBoard();
            lastMoveTime = DateTime.UtcNow;
        }
        public GameSession(GameSession copy)
        {
            sessionBoard = new GameBoard(copy.sessionBoard);
            lastMoveTime = copy.lastMoveTime;
            sessionId = copy.sessionId;
            numberOfPlayers = copy.numberOfPlayers;
        }
        public int numberOfPlayers { get; set; }
        public string       sessionId       { get; set; }
        public GameBoard    sessionBoard    { get; set; }
        public DateTime     lastMoveTime    { get; set; }
        
    }
}