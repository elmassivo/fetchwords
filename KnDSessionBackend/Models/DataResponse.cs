﻿namespace KodeNamesBackend.Models
{
    public class DataResponse<T>
    {
        public bool success
        {
            get
            {
                if (data != null) return true;
                else return false;
            }
        }
        public T    data    { get; set; }
    }
}