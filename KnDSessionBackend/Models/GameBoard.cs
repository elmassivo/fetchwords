﻿using System.Collections.Generic;

namespace KodeNamesBackend.Models
{
    public class GameBoard
    {
        public GameBoard()
        {
            revealedCards   =   new List<int>();
        }
        public GameBoard(GameBoard baseBoard)
        {
            boardSeed = baseBoard.boardSeed;
            revealedCards = baseBoard.revealedCards;
        }
        public string        boardSeed         { get; set; }
        public List<int>     revealedCards     { get; set; }
    }
}