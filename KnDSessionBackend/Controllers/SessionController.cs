﻿using System.Web.Http;
using KodeNamesBackend.Models;
using KodeNamesBackend.Interfaces;
using KodeNamesBackend.Providers;

namespace KnDSessionBackend.Controllers
{
    public class SessionController : ApiController
    {
        readonly ISessionProvider _context = null;
        public SessionController()
        {
            _context = new SessionProvider();
        }

        /// <summary>
        /// Creates a new session using the provided the GameBoard.
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult CreateSession(GameBoard board)
        {
            _context.DestroyOldSessions();
            var response = new DataResponse<GameSession>()
            {
                data = _context.CreateSession(board)
            };
            return Json(response);
        }

        /// <summary>
        /// Retrieves an unexpired session when provided the session Id.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetSession(string sessionId)
        {
            _context.DestroyOldSessions();
            var response = new DataResponse<GameSession>()
            {
                data = _context.GetSession(sessionId)
            };
            return Json(response);
        }
        /// <summary>
        /// Updates the revealed cards of the specified session.
        /// </summary>
        /// <param name="move">Contains sessionId as well as the list of the index of all revealed cards.</param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult PerformMove(PerformMoveParameters move)
        {
            var response = new DataResponse<GameSession>()
            {
                data = _context.PerformMove(move.sessionId, move.revealedCards)
            };
            return Json(response);
        }
        /// <summary>
        /// Updates the board seed and clears all revealed cards of the specified session.
        /// </summary>
        /// <param name="update">Contains sessionId as well as the new board seed.</param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult UpdateSeed(UpdateSeedParameters update)
        {
            var response = new DataResponse<GameSession>()
            {
                data = _context.UpdateSeed(update.sessionId, update.newSeed)
            };
            return Json(response);
        }
    }
}
