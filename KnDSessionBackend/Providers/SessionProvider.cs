﻿using KodeNamesBackend.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using KodeNamesBackend.Models;
using System.Configuration;

namespace KodeNamesBackend.Providers
{
    public class SessionProvider: ISessionProvider
    {
        public static List<GameSession> _sessions = null;
        private static Random rng;

        public SessionProvider(ref List<GameSession> sessionList)
        {
            _sessions = sessionList;
            rng = new Random();
        }

        public SessionProvider()
        {
            rng = new Random();
            if(_sessions == null) _sessions = new List<GameSession>();
        }

        public GameSession GetSession(string sessionId)
        {
            var result = _sessions.FirstOrDefault(u => u.sessionId == sessionId);
            return result;
        }

        public GameSession CreateSession(GameBoard board)
        {
            var result = new GameSession();
            var newSessionId = GetNewSessionId();
            if(GetSession(newSessionId) == null)
            {
                result.sessionId = newSessionId;
            }
            else
            {
                //more than one collision is impressively unlikely
                result.sessionId = GetNewSessionId();
            }
            result.sessionBoard = board;

            _sessions.Add(result);
            return result;
        }

        public GameSession PerformMove(string sessionId, List<int> revealedCards)
        {
            var session = GetSession(sessionId);
            if (session != null)
            {
                session.sessionBoard.revealedCards = revealedCards;
                session.lastMoveTime = DateTime.UtcNow;
            }
            return session;
        }

        public GameSession UpdateSeed(string sessionId, string newBoardSeed)
        {
            var session = GetSession(sessionId);
            if(session != null)
            {
                session.sessionBoard.revealedCards = new List<int>();
                session.sessionBoard.boardSeed = newBoardSeed;
                session.lastMoveTime = DateTime.UtcNow;
            }
            return session;
        }

        public void DestroyOldSessions()
        {
            var sessionTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["sessionTimeout"]);
            var slaughterResult = _sessions.RemoveAll(u=> u.lastMoveTime.AddSeconds(sessionTimeout) < DateTime.UtcNow);
        }

        public string GetNewSessionId()
        {
            string sessionId = null;
            lock (rng)
            {
                string acceptableChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                sessionId = new string(Enumerable.Repeat(acceptableChars, 6).Select(u => u[rng.Next(u.Length)]).ToArray());
            }
            return sessionId;
        }
    }
}