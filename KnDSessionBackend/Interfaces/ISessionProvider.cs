﻿using KodeNamesBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KodeNamesBackend.Interfaces
{
    public interface ISessionProvider
    {
        /// <summary>
        /// Gets a new 6 character session Id string
        /// </summary>
        /// <returns></returns>
        string GetNewSessionId();
        /// <summary>
        /// Creates a new session in the static list using the provided gameboard.
        /// Returns the id of the new session inside the GameSession object.
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        GameSession CreateSession(GameBoard board);
        /// <summary>
        /// Retrieves the GameSession from the static list identified by the sessionId.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        GameSession GetSession(string sessionId);
        /// <summary>
        /// Removes all expired sessions from the static session list.
        /// </summary>
        void DestroyOldSessions();
        /// <summary>
        /// Updates the GameBoard's revealed card list specified by session Id.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="revealedCards"></param>
        /// <returns></returns>
        GameSession PerformMove(string sessionId, List<int> revealedCards);
        /// <summary>
        /// Updates the specified sessions gameboard to use a new seed.
        /// Resets revealed card list.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="newBoardSeed"></param>
        /// <returns></returns>
        GameSession UpdateSeed(string sessionId, string newBoardSeed);

    }
}
