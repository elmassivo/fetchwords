﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KodeNamesBackend.Interfaces;
using KodeNamesBackend.Providers;
using KodeNamesBackend.Models;
using System.Linq;
using System.Configuration;
using System.Threading;
using System.Collections.Generic;

namespace KodeNamesBackend.Tests
{
    [TestClass]
    public class SessionProviderTests
    {
        private readonly ISessionProvider _sessionProvider = null;
        private GameBoard dummyBoard = null;
        public SessionProviderTests()
        {
            _sessionProvider = new SessionProvider();
            createDummyBoard();
        }

        void createDummyBoard()
        {
            dummyBoard = new GameBoard()
            {
                boardSeed = "balformax",
                revealedCards = { 0, 1, 4, 24, 16 },
            };
        }

        [TestMethod]
        public void GetNewSessionId_FunctionalTests()
        {
            var acceptableChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var result = _sessionProvider.GetNewSessionId();
            Assert.IsNotNull(result, "Generated session key is null.");
            Assert.IsTrue(result.Length == 6, "Generated Session key is not the correct length");
            foreach(var c in result)
            {
                Assert.IsTrue(acceptableChars.Contains(c.ToString()), "Generated character ["+c.ToString()+"] is not a valid character.");
            }
        }

        [TestMethod]
        public void CreateSession_FunctionalTests()
        {
            var resultSession = _sessionProvider.CreateSession(dummyBoard);
            var storedSession = _sessionProvider.GetSession(resultSession.sessionId);

            Assert.IsNotNull(resultSession.sessionId, "Created Session is null");
            Assert.AreEqual(resultSession, storedSession, "Created session is not the same as the stored session");
        }

        [TestMethod]
        public void PerformMove_FunctionalTests()
        {
            var move = 9;
            var session = _sessionProvider.CreateSession(dummyBoard);
            var oldlastMoveTime = session.lastMoveTime;
            var boardMove = new GameBoard(session.sessionBoard);
            boardMove.revealedCards.Add(move);

            var resultSession = _sessionProvider.PerformMove(session.sessionId, boardMove.revealedCards);
            
            Thread.Sleep(2000); // sleep for 2 seconds for timer update check.

            var retrievedSession = _sessionProvider.GetSession(session.sessionId);

            Assert.AreEqual(resultSession, retrievedSession, "Returned session is not the same as the stored session.");
            Assert.IsTrue(retrievedSession.sessionBoard.revealedCards.Contains(move), "revealedCards does not contain added card.");
            Assert.AreNotEqual(oldlastMoveTime, resultSession.lastMoveTime, "The value of lastMoveTime is not being changed during performSession.");
        }

        [TestMethod]
        public void PerformMove_InvalidSessionCheck()
        {
            var result = _sessionProvider.PerformMove("NotAnActualSession", new List<int>() { 1, 2, 3 });
            Assert.IsNull(result, "Unassigned Sessions should return null");
        }

        [TestMethod]
        public void UpdateSeed_FunctionalTests()
        {
            var session = new GameSession(_sessionProvider.CreateSession(dummyBoard));
            Thread.Sleep(2000); // sleep for 2 seconds for timer update check.
            var result = _sessionProvider.UpdateSeed(session.sessionId, "newseed!");

            Assert.AreNotEqual(result.sessionBoard.boardSeed, session.sessionBoard.boardSeed, "New board seed is not being set by replacement value.");
            Assert.AreEqual(result.sessionBoard.boardSeed, "newseed!", "New board seed is not being set to the correct value.");
            Assert.IsTrue(result.sessionBoard.revealedCards.Count == 0, "Revealed cards are not being reset on new seed value.");
            Assert.IsTrue(result.lastMoveTime > session.lastMoveTime, "Last Move Time is not being updated on new seed value.");
        }

        [TestMethod]
        public void DestroyOldSessions_FunctionalTests()
        {
            var createdSession = _sessionProvider.CreateSession(dummyBoard);
            var sessionReference = SessionProvider._sessions.FirstOrDefault(u => u.sessionId == createdSession.sessionId);

            Assert.IsNotNull(sessionReference, "Newly created session reference was not found.");

            var sessionTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["sessionTimeout"]);
            sessionReference.lastMoveTime = DateTime.UtcNow.AddHours(-1 * (sessionTimeout+1));
            _sessionProvider.DestroyOldSessions();
            var hopefullyEmptySession = _sessionProvider.GetSession(createdSession.sessionId);

            Assert.IsNull(hopefullyEmptySession, "Old session was not deleted.");
        }

        [TestMethod]
        public void DestroyOldSessions_TimeoutCheck()
        {
            int timeout = 0;
            int.TryParse(ConfigurationManager.AppSettings["sessionTimeout"], out timeout);
            Assert.IsTrue(timeout > 0, "sessionTimeout variable is 0 or missing in app.config.");
        }
    }
}
