# README #

This is a shared state online Codenames clone hosted at https://www.fetchwords.com

# Description #

FetchWords was designed explcitly for my group's DnD campaign on 'off' evenings. 
It was written iteratively over the course of several months as we progressively complained more about the HTML only codenames implementation we had been playing over video chat.

### I've never played codenames before... ###

Here's a short-ish video tutorial on how to play the original boardgame, Codenames: https://www.youtube.com/watch?v=J8RWBooJivg

### Setting up Online Play ###

* Whoever starts the game clicks "CREATE" under the "Create a New Game" section
* Either click "Copy URL" or copy the value of your browser's url field
* Share the URL link in your clipboard with anyone you'd like to play with
* have your co-players pick their team and select a spymaster (FETCHMASTER)
* Click Play
* If you are the Spymaster role, click the hamburger menu on the top-right and click "I AM FETCHMASTER"
* (optional) uncheck "Light Theme" because Kyle demanded it be default and nobody else likes it
	* unchecking it will set a cookie to "never bother me again" intensity
* If you are done with a board or just want a new board, click the "New Board" button
	* All players on the board with you will be automatically moved to the new board

### Features ###

* Shares state and sessions between players
	* Players can change game boards and all visible changes are propogated between players.
* States are joined by short URL string
* Straightforward swagger UI for UI implementation around backend features
* Handles way more games/players simulataneously than anyone will ***ever*** play on this server

### How to Set up Code Locally ###

* Pull, restore nuget packages, and build
